<h1>Instalacion GLPI sobre centos 7 </h1> <br/>
<img src="https://glpi-project.org/wp-content/uploads/2017/03/logo-glpi-bleu-1.png">

<h3>I Instalacion automatizada con Ansible.</h3> <br/>

* **1.- Requisitos para la instalación.** <br/>
yum install -y git ansible

* **2.- Clonar proyecto.** <br/>
git clone https://gitlab.com/ska19/glpi.git

* **3.- Entrar al directorio GLPI.** <br/>
cd glpi/

* **4.- Instalar proyecto ejecutando el siguiente comando.** <br/>
ansible-playbook playbook.yml

Nota: Es importante cambiar el ServerAdmin y ServerName del virtual host posterior al punto 4. <br/>
> Ver Item N° II *"Instalacion Manual"* punto 21. <br/>
> Una vez que se cambia reiniciar httpd "systemctl restart httpd".

* **5.- Entrar al navegador para configurar.** <br/>
http://ip_servidor

* **6.- Configurar.** <br/>
Ver item N° III *"Configuración GLPI".*

* **7.- Notas importantes del autor.** <br/>
        
        SELINUX
 > Es importante reiniciar el servidor para que los cambios aplicados a selinux sean considerados. <br/> 
 > Para saber estado de selinux aplique el siguiente comando: <br/>
 > grep "SELINUX=" /etc/selinux/config |grep -v "#" <br/>
 *> SELINUX=disabled* <br/>

        MYSQL
 > Se crea el usuario "glpiadmin" con la password "glpiadmin" <br/>
 > Recomiendo antes de instalar GLPI cambiar contraseña de usuario glpi por motivos de seguridad. <br/>
 SET PASSWORD FOR 'glpiadmin'@'localhost' = PASSWORD('NUEVA_CONTRASEÑA');FLUSH PRIVILEGES;



<h3>II Instalacion Manual.</h3> <br/>

* **1.- Desactivar politicas de selinux en el archivo de configuración.** <br/>
 vim /etc/selinux/config <br/>
 SELINUX=disabled <br/>
 
 Nota: Para que el cambio tome efecto, es necesario reiniciar el servidor una vez terminada la instalación.


* **2.- Instalar repositorio epel.** <br/>
yum install -y epel-release

* **3.- Crear repositorio MariaDB.** <br/>
vim /etc/yum.repos.d/MariaDB.repo <br/>

[mariadb] <br/>
name = MariaDB <br/>
baseurl = http://yum.mariadb.org/10.4/centos7-amd64 <br/>
gpgkey=https://yum.mariadb.org/RPM-GPG-KEY-MariaDB <br/> 
gpgcheck=1 <br/>

* **4.- Instalar repositorio REMI para instalar PHP 5.6.** <br/>
yum install -y http://rpms.famillecollet.com/enterprise/remi-release-7.rpm 

* **5.- Instalar herramientas necesarias.** <br/>
yum install -y yum-utils vim htop mlocate wget

* **6.- Habilitar repo REMI.** <br/>
yum-config-manager --enable remi-php56

* **7.- Instalar servidor web Apache.** <br/>
yum install -y httpd

* **7.- Habilitar servicio httpd en el arranque.** <br/>
systemctl enable httpd 

* **8.- Iniciar http.** <br/>
systemctl start httpd 

* **9.- Instalar MariaDB y otros paquetes utiles.** <br/>
yum install -y MariaDB-server MariaDB-client

* **10.- Habilitar servicio mariadb en el arranque.** <br/>
systemctl enable mariadb 

* **11.- Iniciar MariaDB.** <br/>
systemctl start mariadb

* **12.- Ejecutar script de seguridad para MariaDB.** <br/>
mysql_secure_installation

* **13.- Instalar php 5.6.** <br/>
yum install -y php php-mcrypt php-cli php-gd php-curl php-mysql php-ldap php-zip php-fileinfo

* **14.- Instalar modulos php para GLPI.** <br/> 
yum install -y php-mbstring php-xml php-imap php-opcache php-apcu php-xmlrpc php-pear-CAS

* **15.- Abrir puertos de firewall.** <br/>
firewall-cmd --permanent --zone=public --add-service=http <br/>
firewall-cmd --permanent --zone=public --add-service=https <br/>
firewall-cmd --reload <br/>

* **16.- Descargar GLPI desde git.** <br/>
wget https://github.com/glpi-project/glpi/releases/download/9.4.5/glpi-9.4.5.tgz

* **17.- Mover archivo a la ruta de Apache.** <br/>
mv glpi-9.4.5.tgz /var/www/html

* **18.- Descomprimir.** <br/>
tar xvf glpi-9.4.5.tgz

* **19.- Asignar permisos.** <br/>
chmod 755 -R /var/www/html/glpi

* **20.- Cambiar propietario.** <br/>
chown -R apache:apache /var/www/html/glpi

* **21.- Crear Virtual Host**. <br/>
vim /etc/httpd/conf.d/glpi.conf

<VirtualHost *:80> <br/>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ServerAdmin alejandro@gmail.com <br/>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ServerName glpi.example.com <br/>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DocumentRoot /var/www/html/glpi <br/>
       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<Directory /var/www/html/glpi> <br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Options Indexes FollowSymLinks MultiViews <br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;AllowOverride All <br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Order allow,deny <br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;allow from all <br/>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Require all granted <br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &lt;/Directory&gt; <br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ErrorLog /var/log/httpd/error-glpi.log <br/>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CustomLog /var/log/httpd/access-glpi.log combined <br/>
&lt;/VirtualHost&gt;

* **22.- Reiniciar servidor.** <br/>
shutdown -r now

<h3>III Configuración GLPI.</h3> <br/>

* **23.- Ingresar al navegador web la IP del servidor.** <br/>
http://ip_servidor


* **24.- Capturas de instalación.** <br/>
![Instalación1](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/1.png "Instalacion 1")<br/>
![Instalación2](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/2.png "Instalacion 2")<br/>
![Instalación3](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/3.png "Instalacion 3")<br/>
![Instalación4](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/4.png "Instalacion 4")<br/>
![Instalación5: Para la version manual](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/5_manual.png "Instalacion 5: Para la version manual")<br/>
![Instalación5: Para la version automatizada](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/5_automatizada.png "Instalacion 5: Para la version automatizada")<br/>
![Instalación6](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/6.png "Instalacion 6")<br/>
![Instalación7](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/7.png "Instalacion 7")<br/>
![Instalación8](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/8.png "Instalacion 8")<br/>
![Instalación9](https://gitlab.com/ska19/imagenes/-/blob/master/glpi/9.png "Instalacion 9")<br/>
![Instalación10](https://gitlab.com/ska19/glpi/-/blob/master/imagenes/10.png "Instalacion 10")<br/>

Nota: Cuando termina la configuración, se debera cambiar las contraseñas por defecto de los 4 usuarios y además eliminar el archivo de instalación *install.php*. <br/>
* **25.- Actualizar Contraseña de los usuarios directo de la DB.** <br/>

Nota: Este punto es opcional ya que se puede hacer directo desde el panel web del servidor GLPI. <br/>
*glpi_users es el nombre de la DB creada como se ve en la imagen Instalacion6*.<br/>
> UPDATE glpi_users SET Password=MD5('Nueva_Contraseña') WHERE name='glpi';<br/>
> UPDATE glpi_users SET Password=MD5('Nueva_Contraseña') WHERE name='tech';<br/>
> UPDATE glpi_users SET Password=MD5('Nueva_Contraseña') WHERE name='normal';<br/>
> UPDATE glpi_users SET Password=MD5('Nueva_Contraseña') WHERE name='post-only';<br/>

* **26.- Eliminar archivo install.php** <br/>
rm -f /var/www/html/glpi/install/install.php


*Dudas o consultas:* <br/>
***aa.valdivialopez@gmail.com***
